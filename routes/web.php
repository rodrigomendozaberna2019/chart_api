<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\ReportPurgeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//  Export files .xlsx
//Route::get('user-list-excel', 'UserController@exportExcel')->name('users.excel');
Route::get('/user-list-excel', [UserController::class, 'exportExcel'])->name('users.excel');
Route::post('/import-list-excel', [ReportPurgeController::class, 'importExcel'])->name('reports.purge.import.excel');
