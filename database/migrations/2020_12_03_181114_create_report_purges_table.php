<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportPurgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_purges', function (Blueprint $table) {
            $table->id();
            $table->text('cdn');
            $table->text('start_time');
            $table->text('title');
            $table->text('program');
            $table->text('happiness_score');
            $table->text('buffer_ratio');
            $table->text('interruptions');
            $table->text('startup_error');
            $table->text('in_stream_error');
            $table->text('type_of_content_displayed');
            $table->text('streaming_protocol');
            $table->text('type');
            $table->text('resource_domain');
            $table->text('media_resource');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_purges');
    }
}
