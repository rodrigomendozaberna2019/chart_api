<?php

namespace App\Imports;

//use App\ReportPurge;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ReportsPurgeImport implements ToModel, WithStartRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new \App\Models\ReportPurge([
            'cdn'                       => $row[0],
            'start_time'                => $row[1],
            'title'                     => $row[2],
            'program'                   => $row[3],
            'happiness_score'           => $row[4],
            'buffer_ratio'              => $row[5],
            'interruptions'             => $row[6],
            'startup_error'             => $row[7],
            'in_stream_error'           => $row[8],
            'type_of_content_displayed' => $row[9],
            'streaming_protocol'        => $row[10],
            'type'                      => $row[11],
            'resource_domain'           => $row[12],
            'media_resource'            => $row[13],
        ]);
    }

    /**
     * @return int
     *
     */
    public function startRow(): int
    {
        return 2;
    }
}
