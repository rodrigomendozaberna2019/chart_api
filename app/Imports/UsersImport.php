<?php

namespace App\Imports;

//use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new \App\Models\User([
            'id'        => $row[0], //a
            'name'      => $row[1], //b
            'email'     => $row[2], //c
            'password'  => $row[3], //d
        ]);
    }
}
