<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Generated document and downloaded in to local
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportExcel() {
        return Excel::download(new UsersExport(), 'user-list.xlsx');
    }

    /**
     * Import document from local
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function importExcel(Request $request) {
        $file = $request->file('file');
        Excel::import(new UsersImport, $file);

        return back()->with('message', 'Importación completa');
    }
}
