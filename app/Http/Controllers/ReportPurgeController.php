<?php

namespace App\Http\Controllers;

use App\Imports\ReportsPurgeImport;
use App\Models\ReportPurge;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportPurgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReportPurge  $reportPurge
     * @return \Illuminate\Http\Response
     */
    public function show(ReportPurge $reportPurge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReportPurge  $reportPurge
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportPurge $reportPurge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReportPurge  $reportPurge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportPurge $reportPurge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReportPurge  $reportPurge
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportPurge $reportPurge)
    {
        //
    }


    public function importExcel(Request $request)
    {
        $file = $request->file('file');
        Excel::import(new ReportsPurgeImport(), $file);

        return back()->with('message', 'Importación completa');
    }
}
