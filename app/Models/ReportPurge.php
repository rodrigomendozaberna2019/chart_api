<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportPurge extends Model
{
    use HasFactory;

    /**
     *The attributes assignable
     *
     */
    protected $fillable = [
        'cdn',
        'start_time',
        'title',
        'program',
        'happiness_score',
        'buffer_ratio',
        'interruptions',
        'startup_error',
        'in_stream_error',
        'type_of_content_displayed',
        'streaming_protocol',
        'type',
        'resource_domain',
        'media_resource',
    ];
}
